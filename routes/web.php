<?php

use App\Models\Invitation;
use Illuminate\Http\Request;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/show', function () {
    return response()->json([
        'status' => true,
        'message' => 'Success',
        'data' => Invitation::latest()->get()
    ], 200);
});

$router->post('/create', function (Request $request) {
    $data = $this->validate($request, [
        'name' => ['required', 'string', 'max:255'],
        'wish' => ['required', 'string', 'max:255'],
        'rsvp' => ['required', 'string', 'max:255', 'in:HADIR,TIDAK'],
    ]);
    return response()->json([
        'status' => true,
        'message' => 'Created',
        'data' => Invitation::create($data)
    ], 201);
});

$router->post('/delete', function (Request $request) {
    $data = $this->validate($request, [
        'id' => ['required', 'numeric', 'exists:invitations,id']
    ]);
    Invitation::find($data['id'])->delete();
    return response()->json([
        'status' => true,
        'message' => 'Deleted',
    ], 200);
});

$router->post('/dee56a21-20b7-4c0f-b7d6-8d0c7e7a940e', function (Request $request) {
    Invitation::truncate();
    return response()->json([
        'status' => true,
        'message' => 'Reseted',
    ], 200);
});

$router->addRoute([
    'HEAD','GET','POST','PUT','PATCH','DELETE','OPTIONS'
],'{any}', function () {
    return response()->json([
        'status' => false,
        'message' => 'Not found',
    ], 404);
});
